package fibonaci;

import java.util.Scanner;

public class InputFromUser {

  private static int a;
  private static int b;
  private static int n;


  InputFromUser() {
  }

  public static int getA() {
    return a;
  }

  public static int getB() {
    return b;
  }

  public static int getN() {
    return n;
  }

  public static void scanNumber() {

    for (int i = 0; i < 3; i++) {

      Scanner sc = new Scanner(System.in);

      try (MyResource m = new MyResource()) {
        if (i == 0) {
          System.out.println("enter start number of range: ");
          if (sc.hasNextInt()) {
            a = sc.nextInt();
          } else {
            throw new WrongInputException("Wrong number, should be integer");
          }
        } else if (i == 1) {
          System.out.println("enter the end number of range: ");
          if (sc.hasNextInt()) {
            b = sc.nextInt();
          } else {
            throw new WrongInputException("Wrong number, should be integer");
          }
        } else {
          System.out.println("enter the length of Fibonacci sequence:");
          if (sc.hasNextInt()) {
            n = sc.nextInt();
          } else {
            throw new WrongInputException("Wrong number, should be integer");
          }
        }
      } catch (Exception e) {
        System.out.println("Exception:" + e.getMessage());
        scanNumber();
      }
    }

  }


}
