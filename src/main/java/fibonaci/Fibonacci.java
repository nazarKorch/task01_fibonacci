package fibonaci;
/**
 * import Scanner
 */

import java.util.Scanner;

/**
 * Fibonacci class.
 */
public final class Fibonacci {

  /**
   * percentage.
   */
  static final int P = 100;

  /**
   * constructor.
   */
  private Fibonacci() {

  }

  /**
   * private Fibonacci() {
   *
   * } /** prints odd and even numbers from interval.
   *
   * @param a beginning of interval.
   * @param b end of interval.
   */
  static void printOddAndEvenNumbers(final int a, final int b) {

    System.out.print("odd numbers: ");
    for (int i = a; i <= b; ) {
      if ((i % 2) != 0) {
        System.out.print(i + " ");
        i += 2;
      } else {
        i++;
      }
    }
    System.out.println();
    System.out.print("even numbers: ");
    for (int j = b; j >= a; ) {
      if ((j % 2 == 0)) {
        System.out.print(j + " ");
        j -= 2;
      } else {
        j--;
      }
    }
    System.out.println();
  }

  /**
   * prints the sum of all numbers from interval.
   *
   * @param a start of range.
   * @param b end of range.
   */
  static void printSum(final int a, final int b) {
    int sum = 0;
    for (int i = a; i <= b; i++) {
      sum += i;
    }
    System.out.println("sum of numbers: " + sum);

  }

  /**
   * prints the biggest odd and biggest even number from sequence.
   *
   * @param n set of numbers.
   */
  static void fibonacciPrint(final int n) {
    int i1 = 0;
    int i2 = 1;
    int countEven = 0;
    int countOdd = 0;
    int f1 = 1;
    int f2 = 0;
    int count = 2;
    int tmp;
    while (n >= count) {
      if (i2 % 2 == 0) {
        f2 = i2;
        countEven++;
      } else {
        f1 = i2;
        countOdd++;
      }
      tmp = i1;
      i1 = i2;
      i2 += tmp;
      count++;
    }
    System.out.println("biggest odd number F1 = " + f1);
    System.out.println("biggest even number F2 = " + f2);

    try {
      System.out.println("percentage of odd numbers: "
          + ((countOdd * P) / n) + "%");
      System.out.println("percentage of even numbers: "
          + ((countEven * P) / n) + "%");
    } catch (Exception e) {
      System.out.println(e.getMessage());
    }

  }


  /**
   * main method.
   *
   * @param args two arguments for interval.
   */
  public static void main(final String[] args) {

    InputFromUser.scanNumber();
    int a = InputFromUser.getA();
    int b = InputFromUser.getB();
    int n = InputFromUser.getN();

    printOddAndEvenNumbers(a, b);
    printSum(a, b);
    fibonacciPrint(n);
  }
}
